# Curriculum Vitae: Robert Scovell
* Citizenship: UK and NZ
* Residency: Thailand
    
## Education
* BSc Physical Science, University of Edinburgh 1991
* BD Hons Systematic Theology, University of Edinburgh 1995
* PGCE (Adult education and training), University of Wales 1996
* Level 3 modules in Statistics, Mathematical Modelling and Computational Maths techniques, Open University, 1996-1998

## Experience in Detail

### Large Perl-based Systems

My most recent relevant client is [Logicly](https://logicly.com.au). I worked with ETLs for surveys based on an in-house adaptation of Dancer. I also created GitHub Workflows.

My previous relevant client project is a large heavily customised Perl-based system for print operations automation that uses the following components.
I actively develop each of these components. The customised aspects have evolved over 20 years so the application contains multi-generational perl code.
The owner's philosophy  (which I share) is for us to keep up to date with Perl conventions and code new modules accordingly. The code is meticulously reviewed and tested --
there is a strong motivation with such a mature and long-term code-base to avoid technical debt.

* `DBIx::Class` heavily expanded with a heap of custom features specific to the client's unique operational requirements. There are 136 Model classes
with associated Controllers and Action handling classes.
* `Dist::Zilla` for automated builds run by Ansible.
* Jenkins for automated building and testing triggered by new commits to the GitHub repository.
* `Test::More` test harness. In the project there are 4925 tests. This is a project where Test-Driven Development is a reality, not a 'nice idea maybe later'.
  The test process builds a completely fresh database from the current schema and loads fresh test data before the tests start running. Every ticket from
  the client includes either new test data or a clear spec for test data.
* Slack integration. I created a Slackbot that monitors Jenkins and alerts us when a commit's build fails. In then alerts us again when the build succeeds.
* `HTML::Mason` for the View components.
* Custom OO framework. The application uses a custom OO framework -- not Moose but similar. The owner had some specific architectural requirements
that led him to reject both frameworks in favour of his own, which works incredibly well given his particular requirements. The MVC structure of the system
is built on this OO framework, his extended `DBIx::Class` framework and `HTML::Mason`.
* `vue.js` front end (migrating from legacy jQuery and some Angular).
* PostgreSQL.

As part of this project I built a CPAN Perl-XS module for importing PNG files with transparency into PDF files because the pure Perl version was incredibly slow.

* [ImagePNG.xs on CPAN](https://metacpan.org/release/SSIMMS/PDF-API2-XS-1.002/source/lib/PDF/API2/XS/ImagePNG.xs)
* [ImagePNG.xs GitHub](https://github.com/robscovell-ts/pdfapi2xs)

For 10 years (2004-2014) I developed custom VoIP billing applications using the PortaONE telephony billing platform. This platform was build on OO Perl modules
and the `HTML::Mason` framework. During that time I also worked for smaller clients on `HTML::Mason` projects.

### Anomaly Detection in Operational Data ETL pipelines
I recently built an ETL (in Java) with anomaly/fraud detection for a VoIP client.

## Less Formal Training
I am continually researching and learning new technologies and techniques:

* Erlang, BEAM and the intricacies of message passing+parsing in a massively distributed environment.
* Functional Programming: Over the last few years I have focused on bringing functional programming techniques into my regular development work.
I have done some experiments with ETLs using Clojure.
* Anomaly Detection: I have taken a deep dive into the work of Nicholas Nassim Taleb around power-law statistics, with a focus on anomaly detection and outlier analysis. This is an area I want to expand into.

## Contract Work Experience (reverse order)
* [Logicly](https://logicly.com.au) 2022 -- 2024
* [TechSound](http://www.techsound.com) 2020 -- 2022
* [Company-X](https://companyx.com/) 2012 -- present
* [Vonage UK](https://www.vonage.co.uk/) 2014 -- present
* [Hi Voicemail](https://hivoicemail.com/) 2016
* [Coms plc](https://www.timico.com/) (former VoIP company subsequently taken over by Timico) 2004 -- 2014
* [Collins Dictionaries](https://www.collinsdictionary.com/) 1999 -- 2004
* [National Centre for Prosthetics and Orthotics](https://www.strath.ac.uk/courses/undergraduate/prostheticsorthotics/) 1998 -- 1999
* [Pfizer UK](https://www.pfizer.co.uk/) 1997 -- 1998
    
## Relevant Project experience
* General: I work with Company-X supporting a number of legacy applications developed in  Perl, PHP, MySQL, PostgreSQL, MS SQL, Delphi etc.
* Vonage U.S.: I built and currently support an anomaly detection system to prevent fraud.
* TechSound: I create new operations automation modules as required. 
* Hi Voicemail: I built a voicemail server using Node.js and Freeswitch.
* Vonage UK: I support, maintain and develop a set of Perl applications that are mission-critical for the company's VOIP operations. 
* Coms plc: from 2004 to 2014 I developed, supported and maintained a VOIP billing system written using Perl/Mason.
* Troy Reasearch: from 2004 to 2007 I developed, supported and maintained OO-Perl and embperl code.
* Collins Dictionaries: from 1999 to 2004 I worked for Collins Dictionaries on Perl text processing pipelines for typesetting dictionaries from tagged text through to PostScript.
* National Centre for Prosthetics and Orthotics: Delphi, BDE and Paradox application for hospital amputee treatment data gathering. SPSS for statistical analysis.
* Pfizer Pharmaceuticals UK: my first contract was writing test scripts in Fortran and Genstat. I used Perl to process output from the test scripts into RTF to create Word documents.
    
## Business Modus Operandi
My main guiding principle in business is to align myself with the objectives of the people who are paying me. This means working with and for people whose 
values and ethics I can align with. It also means that I always look beyond the tasks in front of me to how those tasks align with the objectives of the business.

My second guiding principle is to always do what I promise to do, or to put it another way, only promise that which I can deliver on. I estimate tasks using
a combination of a complexity score and an unpredictability score, using the Fibonacci scoring method common in Agile sprint planning. The complexity score
is an estimate of how much time is required to understand the known aspects of the problem. The unpredictability score is a measure of how much research into
unknowns I think will be required. I am fully transparent about what I think a particular task will involve.

My approach to software development is to spend time to fully understand the problem that the task solves, building up a complete mental model of the relevant data, 
error conditions, risks, etc., before I even touch the keyboard to write code. This includes understanding how the task fits into the project's objectives as a whole.
The discipline of coding in this way results in clean, careful, well-reasoned code.

I generally have a data-first approach to understanding a task or problem. I like to play with the data, discover anomalies, think through what happens if
fields are missing, etc. Then when I have that thoroughly nailed I start to conceptualise how the data relates to the business rules that need to be coded into
the application. This leads into deciding on test data, building tests, and then building the code to pass the tests.

## Downtime
At weekends I love to just head out to the mountains with my family and just chill.
