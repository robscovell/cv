# Curriculum Vitae: Robert Scovell

## Education
* BSc Physical Science, University of Edinburgh 1991
* BD Hons Systematic Theology, University of Edinburgh 1995
* PGCE (Adult education and training), University of Wales 1996
* Level 3 modules in Statistics, Mathematical Modelling and Computational Maths techniques, Open University, 1996-1998

## Contract Work Experience (reverse order)
* [Logicly](https://www.logicly.com.au/) 2021 -- present
* [Company-X](https://companyx.com/) 2012 -- present
* [Vonage UK](https://www.vonage.co.uk/) 2014 -- present
* [MEA](https://we-are-mea.com/) 2007 -- 2009
* [Coms plc](https://www.timico.com/) (former VoIP company subsequently taken over by Timico) 2004 -- 2014
* [Collins Dictionaries](https://www.collinsdictionary.com/) 1999 -- 2004
* [National Centre for Prosthetics and Orthotics](https://www.strath.ac.uk/courses/undergraduate/prostheticsorthotics/) 1998 -- 1999
* [Pfizer UK](https://www.pfizer.co.uk/) 1997 -- 1998

## What I Do
With my background in statistics and data modelling, and my expertise in understanding and designing deep data structures, I specialise in ETL-type systems.

## Languages
* Python (inc. Pandas)
* Perl (including OO Perl)
* PHP
* SQL
* R
* bash scripting

## Technologies
* AWS
* Docker
* GitHub Actions
* mySQL, PostgreSQL, MS SQL Server
* Linux/Unix servers
* Microsoft PowerBI
* Tableau
* Azure Data Factory
* R Studio